export "COLORFGBG=15;0"
setxkbmap -option caps:swapescape
neofetch
# export PATH=$HOME/bin:/usr/local/bin:$PATH
export MANPATH="/usr/local/man:$MANPATH"
export LANG=en_US.UTF-8
export EDITOR='vim'

#vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
# Vars
#vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
HISTFILE=~/.zsh_history
SAVEHIST=1000
setopt inc_append_history # To save every command before it is executed
setopt share_history # setopt inc_append_history

#vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
# Aliases
#vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
alias a='alias'
a v='vim'
a vi='vim'
a la='ls -a'
a lt="ls -alrt --color"
a ll='ls -l'
a lla='ls -al'
a lh="ls -lrhF -S --color"
a lg="lt | grep ";
a gh='history | grep'
a tmux='TERM=xterm-256color tmux'
a p1="awk '{print \$1}'"
a p2="awk '{print \$2}'"
a p3="awk '{print \$3}'"
a grep='grep -i'
a h="history"
a gp="grep -P "
a p="pwd"
a tf="tail -f "
a disk="/home/eboren/cde/sw/bin/disk "
a mypath="echo $PATH | tr : \"\n\""
a du1="du -h --max-depth=1"
a du2="du -h --max-depth=2"
a ad='cd ~/MEGA/Games/Quake && vkquake -game ad >/dev/null 2>&1 &'
a battle='WINEDEBUG=-ALL wine ~/.wine/drive_c/Program\ Files\ \(x86\)/Battle.net/Battle.net\ Launcher.exe >/dev/null 2>&1 &'
a bdoom='cd /usr/share/gzdoom/ && brutal-doom >/dev/null 2>&1 &'
a blood='cd ~/MEGA/Games/Blood && java -jar BloodGDX.jar >/dev/null 2>&1 &'
a calcurse='calcurse -D ~/MEGA/MEGAsync/Calcurse'
a chrome='google-chrome-stable 2>/dev/null &'
a darkplaces='darkplaces-glx -basedir ~/MEGA/Games/Quake > /dev/null &'
a g++='g++ -std=c++11'
a gamecube='dolphin-emu >/dev/null 2>&1 &'
a n64='mupen64plus'
a pandora='pianobar'
a psp='ppsspp-qt >/dev/null 2>&1 &'
a quake='cd ~/MEGA/Games/Quake && vkquake >/dev/null 2>&1 &'
a shock2='cd /home/evan/.local/share/Steam/steamapps/common/SS2/support/systemshock2/drive_c/SystemShock2/ && WINEDEBUG=-ALL wine Shock2 >/dev/null 2>&1 &'
a steam-wine='WINEDEBUG=-all wine ~/.wine/drive_c/Steam/Steam.exe >/dev/null 2>&1 &'
a ya='yaourt'
a sgames='cd ~/.steam/steam/steamapps/common'

#vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
# Settings
#vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
export VISUAL=vim

source ~/dotfiles/zsh/plugins/fixls.zsh

#vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
#Functions
#vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
# Custom cd
c() {
    cd $1;
    ls;
}

# For vim mappings:
stty -ixon

#source ~/dotfiles/zsh/plugins/oh-my-zsh/lib/history.zsh
#source ~/dotfiles/zsh/plugins/oh-my-zsh/lib/key-bindings.zsh
#source ~/dotfiles/zsh/plugins/oh-my-zsh/lib/completion.zsh
source ~/dotfiles/zsh/plugins/vi-mode.plugin.zsh
source ~/dotfiles/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
source ~/dotfiles/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
#source ~/dotfiles/zsh/keybindings.sh

#vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
# Fix for arrow-key searching
# start typing + [Up-Arrow] - fuzzy find history forward
#vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
if [[ "${terminfo[kcuu1]}" != "" ]]; then
    autoload -U up-line-or-beginning-search
    zle -N up-line-or-beginning-search
    bindkey "${terminfo[kcuu1]}" up-line-or-beginning-search
fi

#vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
# start typing + [Down-Arrow] - fuzzy find history backward
#vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
if [[ "${terminfo[kcud1]}" != "" ]]; then
    autoload -U down-line-or-beginning-search
    zle -N down-line-or-beginning-search
    bindkey "${terminfo[kcud1]}" down-line-or-beginning-search
fi

#vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
# Compilation flags
#vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
export ARCHFLAGS="-arch x86_64"

#vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
# ssh
#vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
export SSH_KEY_PATH="~/.ssh/rsa_id"

#vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
# prompt
#vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
autoload -U colors && colors
PS1="%B%F{172}[%f%F{092}%~%f%F{172}]: %b%f"

#vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
# autoload tmux
#vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
#time_out () { perl -e 'alarm shift; exec @ARGV' "$@"; }

# Run tmux if exists
#if command -v tmux>/dev/null; then
    #[ -z $TMUX ] && exec tmux
#else
    #echo "tmux not installed. Run ./deploy to configure dependencies"
#fi
