#vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
# General Config
#vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
#neofetch
#pfetch
~/dotfiles/scripts/fgel.sh
export LANG='en_US.UTF-8'
export "COLORFGBG=15;0"
export EDITOR='vim'
set -o vi

#vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
# Swap Capslock and Escape keys
#vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
setxkbmap -option caps:swapescape

#vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
# Autoload tmux
#vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
#if command -v tmux>/dev/null; then
#    [ -z $TMUX ] && exec tmux
#else
#    echo "tmux not installed. Run ./deploy to configure dependencies"
#fi


#vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
# Aliases
#vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
alias a='alias'
a v='vim'
a vi='vim'
a ls='ls --color=tty'
a la='ls -a'
a lt="ls -alrt --color"
a ll='ls -l'
a lla='ls -al'
a lh="ls -lrhF -S --color"
a lg="lt | grep ";
a gh='history | grep'
a tmux='TERM=xterm-256color tmux'
a p1="awk '{print \$1}'"
a p2="awk '{print \$2}'"
a p3="awk '{print \$3}'"
a grep='grep -i'
a h="history"
a gp="grep -P "
a p="pwd"
a tf="tail -f "
a disk="/home/eboren/cde/sw/bin/disk "
a mypath="echo $PATH | tr : \"\n\""
a du1="du -h --max-depth=1"
a du2="du -h --max-depth=2"
a ad='cd ~/media/mega/Games/Quake && quakespasm -game ad >/dev/null 2>&1 &'
a bdoom='brutal-doom >/dev/null 2>&1 &'
a blood='cd ~/media/mega/Games/Blood && java -jar BloodGDX.jar >/dev/null 2>&1 &'
a darkplaces='darkplaces -basedir ~/media/mega/Games/Quake > /dev/null &'
a gamecube='dolphin-emu >/dev/null 2>&1 &'
a n64='mupen64plus'
a pandora='pianobar'
a psp='ppsspp-qt >/dev/null 2>&1 &'
a quake='quakespasm -basedir /home/evan/Games/Quake >/dev/null 2>&1 &'
a quake2='yamagi-quake2 -datadir /home/evan/Games/Quake2 >/dev/null 2>&1 &'
a shock2='cd /home/evan/.local/share/Steam/steamapps/common/SS2/support/systemshock2/drive_c/Program\ Files/SystemShock2/ && WINEDEBUG=-ALL wine Shock2 >/dev/null 2>&1 &'
a steam-wine='WINEDEBUG=-all wine ~/.wine/drive_c/Steam/Steam.exe >/dev/null 2>&1 &'
a ya='yaourt'
a sgames='cd ~/.steam/steam/steamapps/common'
a dosbox='dosbox-x'
a cdoom2='crispy-doom -iwad doom2.wad >/dev/null 2>&1 &'
a cdoom='crispy-doom -iwad doom.wad -af sigil.wad >/dev/null 2>&1 &'
a hedon='cd /home/evan/media/mega/Games/Doom/Hedon && ./Launch\ Hedon >/dev/null 2>&1 &'

#vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
# Prompt Codes
#vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
CLEAR="\[\033[0m\]"
BOLD="\[\033[1m\]"
BLACK="\[\033[0;30m\]"
DKGRAY="\[\033[1;30m\]"
RED="\[\033[0;31m\]"
GREEN="\[\033[0;32m\]"
BROWN="\[\033[0;33m\]"
YELLOW="\[\033[1;33m\]"
BLUE="\[\033[0;34m\]"
DKBLUE="\[\033[1;34m\]"
CYAN="\[\033[0;36m\]"
DKCYAN="\[\033[1;36m\]"
WHITE="\[\033[0;37m\]"
GRAY="\[\033[1;37m\]"
TITLE_BAR="\[\e]2;\w\a"

#vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
# Prompt
#vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv

export PS1="\[\e[38;5;27m\][\[\e[m\]\[\e[38;5;201m\]\u\[\e[m\]\[\e[00;34m\]@\[\e[m\]\[\e[38;5;201m\]\h\[\e[m\]\[\e[00;34m\]:\[\e[m\]\[\e[38;5;87m\]\W\[\e[m\]\[\e[38;5;27m\]]\[\e[m\]\[\e[38;5;27m\]\\$\[\e[m\] "

#vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
# Compilation flags
#vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
export ARCHFLAGS="-arch x86_64"

#vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
# ssh
#vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
export SSH_KEY_PATH="~/.ssh/"

#vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
#Functions
#vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
# Custom cd
function c() {
    builtin cd "$@" && ls
}
