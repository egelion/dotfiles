#!/bin/bash

function run {
    if ! pgrep $1 ;
    then
        $@&
    fi
}

run sxhkd &
feh --bg-fill ~/Pictures/Alone.png
run picom -b
~/.config/polybar/launch.sh

run megasync &
xscreensaver -no-splash &   
# numlockx on &
xrandr --output DP-0 --mode 2560x1440 -r 144
