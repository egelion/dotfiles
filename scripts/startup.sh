#!/bin/bash

if [ -z "$(pgrep feh)" ] ; then
	feh --bg-fill ~/Pictures/Alone.png
fi

if [ -z "$(pgrep picom)" ] ; then
	picom -b
fi

if [ -z "$(pgrep xsreensaver)" ] ; then
	xscreensaver -no-splash &
fi

if [ -z "$(pgrep powerkit)" ] ; then
    powerkit &
fi

fixMouse
numlockx on &
xrandr --output DP-0 --mode 2560x1440 -r 144
