#!/bin/bash

if [ -z "$(pgrep feh)" ] ; then
    feh --bg-fill ~/Pictures/Alone.png
fi

if [ -z "$(pgrep picom)" ] ; then
    picom -b
fi

if [ -z "$(pgrep xsreensaver)" ] ; then
    xscreensaver -no-splash &
fi

if [ -z "$(pgrep pa-applet)" ] ; then
    pa-applet >/dev/null 2>&1 &
fi

if [ -z "$(pgrep nm-applet)" ] ; then
    nm-applet >/dev/null 2>&1 &
fi

if [ -z "$(pgrep megasync)" ] ; then
    megasync >/dev/null 2>&1 &
fi

if [ -z "$(pgrep powerkit)" ] ; then
    powerkit &
fi

fixMouse
numlockx on &
xrandr --output DisplayPort-0 --mode 2560x1440 -r 144
