""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" General Configuration
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let mapleader=","
execute pathogen#infect()
filetype plugin indent on
syntax on
set encoding=utf-8
set t_Co=256
colorscheme gruvbox
highlight Normal ctermbg=NONE
highlight nonText ctermbg=NONE
set tabstop=4
set autoindent
set smartindent
set smarttab
set expandtab
set shiftwidth=4
set nu
set relativenumber
set ic
set dir=/tmp/swap,/home/eboren/tmp/vimSwap,.
set foldmethod=indent
set foldlevel=99
set cursorline
set hlsearch
set listchars=tab:\|\
set mouse=a
set incsearch
set laststatus=2
let python_highlight_all=1
let NERDTreeShowHidden=1
"hi Cursor ctermfg=White ctermbg=Yellow cterm=bold guifg=white guibg=yellow gui=bold
set listchars=eol:$,tab:»·,trail:~,extends:>,precedes:<,nbsp:☠

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Parenthesis/quote matching (allows for typing the ending match at the end of the
" line when needed)
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
inoremap ( ()<Esc>i
inoremap [ []<Esc>i
inoremap < <><Esc>i
inoremap { {}<Esc>i
autocmd Syntax html,vim inoremap < <lt>><Esc>i| inoremap > <c-r>=ClosePair('>')<CR>
inoremap ) <c-r>=ClosePair(')')<CR>
inoremap ] <c-r>=ClosePair(']')<CR>
inoremap } <c-r>=ClosePair('}')<CR>
inoremap > <c-r>=ClosePair(">")<CR>
inoremap " <c-r>=QuoteDelim('"')<CR>
inoremap ' <c-r>=QuoteDelim("'")<CR>
inoremap {<CR> {<CR>}<C-o>O
"inoremap <CR> <ESC>:call CloseBracket()<CR>

function ClosePair(char)
    if getline('.')[col('.') - 1] == a:char
    return "\<Right>"
    else
    return a:char
    endif
endf

function CloseBracket()
    if getline('.')[col('.')] == '}'
        substitute /\s*$//
        exec "normal! $xA\<Cr>X\<Cr>}\<Esc>k$r\<Space>"
    else
        let endl = (col('.') != col('$')-1)
        if endl == 1
            exec "normal! a\<CR>\<ESC>l"
        else
            exec "normal! o"
        endif
    endif
    :startinsert
endfunction

function QuoteDelim(char)
    let line = getline('.')
    let col = col('.')
    if line[col - 2] == "\\"
    return a:char
    elseif line[col - 1] == a:char
    return "\<Right>"
    else
    return a:char.a:char."\<Esc>i"
    endif
endf

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" File and Window Management
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
inoremap <leader>w <Esc>:w<CR>
nnoremap <leader>w :w<CR>
inoremap <leader>W <Esc>:wq<CR>
nnoremap <leader>W :wq<CR>

inoremap <leader>q <ESC>:q<CR>
nnoremap <leader>q :q<CR>
inoremap <leader>Q <ESC>:q!<CR>
nnoremap <leader>Q :q!<CR>

inoremap <leader>x <ESC>:x<CR>
nnoremap <leader>x :x<CR>

nnoremap <leader>e :Ex<CR>
nnoremap <leader>t :tabnew<CR>:Ex<CR>
nnoremap <leader>v :vsplit<CR>:w<CR>:Ex<CR>
nnoremap <leader>s :split<CR>:w<CR>:Ex<CR>

nnoremap <C-J> <C-W><C-J>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-H> <C-W><C-H>

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Remapping function keys to execute functions
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"type z= over a highlighted word to get spelling suggestions
nnoremap <F3> :set spell spelllang=en_us<CR>
nnoremap <F4> :set nospell<CR>
autocmd FileType python nnoremap <buffer> <F5> :w<CR>:exec '!clear; python3' shellescape(@%, 1)<CR>
nnoremap <F6> :NERDTree<CR>
nnoremap <F7> :call Tex_RunLaTeX()<CR>
" F9 Removes all trailing whitespace
nnoremap <F9> :let _s=@/<Bar>:%s/\s\+$//e<Bar>:let @/=_s<Bar><CR>
nnoremap <F11> :set invpaste<CR>
set pastetoggle=<F11>

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Swap split panes
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
function! MarkWindowSwap()
    let g:markedWinNum = winnr()
endfunction

function! DoWindowSwap()
    " Mark Destination
    let curNum = winnr()
    let curBuf = bufnr("%")
    exe g:markedWinNum . "wincmd w"
    " Switch to source and shuffle dest->source
    let markedBuf = bufnr("%")
    " Hide and open so taht we aren't prompted and keep history
    exe 'hide buf' curBuf
    " Switch to dest and suffle source->dest
    exe curNum . "wincmd w"
    " Hide and open so that we aren't prompted and keep history
    exe 'hide buf' markedBuf
endfunction

nmap <silent> <leader>mw :call MarkWindowSwap()<CR>
nmap <silent> <leader>pw :call DoWindowSwap()<CR>

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Misc Mappings
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
inoremap <leader>for <esc>Ifor (int i = 0; i < <esc>A; i++) {<enter>}<esc>O<tab>
inoremap <leader>if <esc>Iif (<esc>A) {<enter>}<esc>O<tab>

" <leader>s will search and replace the word under the cursor type a g to replace all
nnoremap <leader>R :%s/\<<C-r><C-w>\>/

map <tab> %
nnoremap <C-c> :nohl<CR><C-c>:echo "Search Cleared"<CR>
nnoremap <leader><tab> :set list!<CR>

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Airline Configuration
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:airline_powerline_fonts=1
"set timeoutlen=10
let g:airline_theme='gruvbox'
let loaded_matchparen=1

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Vim-LaTeX Configuration
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:vimtex_compiler_latexmk = {'callback' : 0}
set grepprg=grep\ -nH\ $*
let g:tex_flavor = "latex"
let g:Tex_DefaultTargetFormat = 'pdf'
let g:Tex_CompileRule_pdf = 'latexmk -pdf -pv -g'
"let g:latex_viewer = '/usr/lib/evince'
let g:Tex_ViewRule_pdf = 'evince'

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Return to the same line at which you left
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
augroup line_return
    au!
        au BufReadPost *
        \ if line("'\"") > 0 && line("'\"") <= line("$") |
            \ execute 'normal! g`"zvzz' |
        \ endif
augroup END

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Notes
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" To swap escape and capslock keys in xserver, use this line in .xinitrc
" setxkbmap -option caps:swapescape
"
" multicursor keybindings:
"   start: <C-n> start multicursor and add a virtual cursor + selection on the
"     match
"   next: <C-n> add a new virtual cursor + selection on the next match
"   skip: <C-x> skip the next match
"   prev: <C-p> remove the currect virtual cursor + selection and go back to
"     prevous match
"   select all: <A-n> start multicursor and directly select all matches
"
